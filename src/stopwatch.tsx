import React from 'react';
import './stopwatch.css';

interface Props {}

interface State {
	startTime: number,
	elapsedTime: number,
	intervalId: number,
	displayTime: string,
	isRunning: boolean,
	isPaused: boolean
}

class Stopwatch extends React.Component<Props,State> {

	constructor(props: Props){
		super(props)

		this.state = {
			startTime: 0,
			elapsedTime: 0,
			intervalId: 0,
			displayTime: "00:00:00",
			isRunning: false,
			isPaused: false
		}
	}

	timeToString = (time: number): string => {
	  let diffInHrs = time / 3600000;
	  let hh = Math.floor(diffInHrs);

	  let diffInMin = (diffInHrs - hh) * 60;
	  let mm = Math.floor(diffInMin);

	  let diffInSec = (diffInMin - mm) * 60;
	  let ss = Math.floor(diffInSec);

	  let diffInMs = (diffInSec - ss) * 100;
	  let ms = Math.floor(diffInMs);

	  let formattedMM = mm.toString().padStart(2, "0");
	  let formattedSS = ss.toString().padStart(2, "0");
	  let formattedMS = ms.toString().padStart(2, "0");

	  return `${formattedMM}:${formattedSS}:${formattedMS}`;
	}

	startWatch = () => {

		const { isRunning, isPaused, intervalId } = this.state;

		if( (!isRunning && !isPaused) || (isRunning && isPaused)){
			this.setState(({elapsedTime}) => ({
				startTime: Date.now() - elapsedTime
			}));

			const newInterval : number = window.setInterval( () => {
				this.setState(({startTime}) => ({ 
					elapsedTime: Date.now() - startTime , 
					displayTime: this.timeToString(Date.now() - startTime)
				}))
			}, 10)

			if(isPaused)
				this.setState({
					intervalId: newInterval, 
					isRunning: true, 
					isPaused: false
				})
			else
				this.setState({
					intervalId: newInterval, 
					isRunning: true
				})

		} else if (isRunning && !isPaused){
			this.setState({ isPaused: true})
			clearInterval(intervalId)
		}
	}

	stopWatch = () => {
		const {intervalId} = this.state;
		clearInterval(intervalId)

		this.setState({
			elapsedTime : 0, 
			intervalId: 0, 
			displayTime: "00:00:00", 
			isRunning: false, 
			isPaused: false
		})
	}

	componentWillUnmount(){
		const {intervalId} = this.state;
		clearInterval(intervalId)
	}

	render(){

		const { isRunning, isPaused, displayTime } = this.state;

		return (
			<div className="stopwatch">
				<div className="stopwatch-heading"> STOP WATCH </div>
				<div className="stopwatch-time"> {displayTime} </div>
				<div className="stopwatch-buttons">
					<div className="stopwatch-button" onClick={() => this.startWatch()}> {isRunning ? (isPaused ? 'Resume' : 'Pause') : "Start"} </div>
					<div className="stopwatch-button" onClick={() => this.stopWatch()}> Stop </div>
				</div>
			</div>
		)
	}
}

export default Stopwatch